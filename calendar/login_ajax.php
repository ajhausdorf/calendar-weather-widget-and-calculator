<?php
// This is a *good* example of how you can implement password-based user authentication in your web application.


header("Content-Type: application/json");

require 'calendar_database.php';

$user = $_POST['username'];
$pwd_guess = $_POST['password'];
 
// Use a prepared statement
$stmt = $mysqli->prepare("SELECT user_id, password_encrypted FROM users WHERE username=?");
 
 		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
			}

// Bind the parameter
// $user = $_POST['username'];
$stmt->bind_param('s', $user);
$stmt->execute();
 
// Bind the results
$stmt->bind_result($user_id, $pwd_hash);
$stmt->fetch();


// $pwd_guess = $_POST['password'];
// Compare the submitted password to the actual password hash
if(crypt($pwd_guess, $pwd_guess)==$pwd_hash){
	// Login succeeded!
	session_start();
	$_SESSION['username'] = $user;
	$_SESSION['token'] = substr(md5(rand()), 0, 10);

	//ajax logging a user in.
	echo json_encode(array(
		"success" => true,
		"user" => $_SESSION['username'],
		"isLoggedIn" => true,
		));
	exit;
	
}else{
	echo json_encode(array(
		"success" => false,
		"isLoggedIn" => false,
		"message" => "Incorrect Username or Password"
		));
	exit;
}
?>