<?php

	require 'calendar_database.php';
	header("Content-Type: application/json");

	//pulling data from the form entered
	$username = $_POST['username'];
	$password = crypt($_POST['password'], $_POST['password']);


	//check length of the username and password entered.. makes sure it is valid before proceeding
	// if(strlen($username) <= 0 || strlen($password) <= 0) {
	// 	echo json_encode(array(
	// 		"success" => false,
	// 		"isLoggedIn" => false,
	// 		"message" => "failing early"
	// 		));
	// 	exit;
	// }

	// inserting information from the form into our users table
	$stmt = $mysqli->prepare("insert into users (username, password_encrypted) values (?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		echo "Sorry, that username was too long or already taken";

		echo json_encode(array(
			"success" => false,
			"isLoggedIn" => false,
			"message" => "Invalid username or password."
			));
		exit;
	}


	 

	$stmt->bind_param('ss', $username, $password);
	 
	$stmt->execute();

	$stmt->close();

	//check to see if query worked.. if it did start the session with the user.

		session_start();
		$_SESSION['username'] = $username;
		$_SESSION['token'] = substr(md5(rand()), 0, 10);

		echo json_encode(array(
			"success" => true,
			"isLoggedIn" => true,
			"user" => $_SESSION['username']
			));
		exit;


 


 
?>